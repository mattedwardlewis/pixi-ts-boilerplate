import * as PIXI from 'pixi.js';

const canvasWidthHeight = 512;
const BIRD_FRAME_LIST = [
	'./src/assets/images/bird-1.png',
	'./src/assets/images/bird-2.png',
	'./src/assets/images/bird-3.png',
	'./src/assets/images/bird-4.png',
];

class Bird {
	private sprite = new PIXI.Sprite();
	private textureCounter: number = 0;

	constructor(pixiStage: PIXI.Container) {
		pixiStage.addChild(this.sprite);
		this.sprite.scale.x = 0.06;
		this.sprite.scale.y = 0.06;
		// Set the transformation origin
		this.sprite.anchor.set(0.5, 0.5);
		this.sprite.anchor.set(0.5, 0.5);
		this.reset();
	}

	public reset() {
		this.sprite.x = canvasWidthHeight / 6;
		this.sprite.y = canvasWidthHeight / 2.5;
	}

	public draw = () => {
		this.sprite.texture = PIXI.loader.resources[BIRD_FRAME_LIST[this.textureCounter++]].texture;

		if (this.textureCounter === BIRD_FRAME_LIST.length) {
			this.textureCounter = 0;
		}
	}
}

const renderer = PIXI.autoDetectRenderer(canvasWidthHeight, canvasWidthHeight);
document.body.appendChild(renderer.view);
const stage = new PIXI.Container();

PIXI.loader
	.add(BIRD_FRAME_LIST)
	.load(setup);

let bird: Bird;
const FPS: number = 200;
let t: number = Date.now();

const button = document.querySelector('#start');
function setup() {
	bird = new Bird(stage);
	requestAnimationFrame(draw);
}

function draw() {
	const dt: number = Date.now() - t;
	if (dt >= 8) {
		bird.draw();
	}

	renderer.render(stage);

	t += dt;

	requestAnimationFrame(draw);
}
